package exercise.logging;

import creational.singleton.SessionData;
import sun.rmi.runtime.Log;

import java.sql.SQLOutput;

public class Logger {
    private static Logger instance = null;

    private Logger(){

    }
    public void logThis(String message) {
        System.out.println(message);
    }


    public static Logger getInstance() {
        if (instance == null) {
            instance = new Logger();
        }
        return instance;
    }

}
