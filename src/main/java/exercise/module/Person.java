package exercise.module;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Person implements Personal{
    protected String firstname;
    protected String lastname;
    protected Date dateOfBirth;


    public Person(String firstname, String lastname, Date dateOfBirth){
        this.firstname = firstname;
        this.lastname = lastname;
        this.dateOfBirth = dateOfBirth;
    }

}
