package exercise.module;

import java.util.Date;
import java.util.Random;

public class PersonFactory implements Personal{

    static String[] firstName = {"First","Second","Third","Fourth","Fifth"};
    static String[] lastName = {"First","Second","Third","Fourth","Fifth"};

    public static Person createPerson(personalType type) {
        Integer randomFirstNameIndex = new Random().nextInt();
        Integer randomLastNameIndex = new Random().nextInt();
        switch (type) {
            case STUDENT:
                return new Student(firstName[randomFirstNameIndex],lastName[randomLastNameIndex],new Date());
            case TRAINER:
                return new Trainer(firstName[randomFirstNameIndex],lastName[randomLastNameIndex],new Date());
            default:
                return null;
        }
    }
}
