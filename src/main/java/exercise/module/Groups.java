package exercise.module;

import java.util.ArrayList;
import java.util.List;

public class Groups {
    private String name;
    Trainer trainer;
    List<Student> studentList = new ArrayList<>();

    public void setName(String name) {
        this.name = name;
    }

    public void assignTrainer(Trainer trainer) {
        this.trainer = trainer;

    }

}
