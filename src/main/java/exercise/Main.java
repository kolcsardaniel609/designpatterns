package exercise;

import exercise.logging.Logger;
import exercise.module.Person;
import exercise.module.PersonFactory;
import exercise.module.Personal;
import exercise.module.Student;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        Logger sd = Logger.getInstance();
        sd.logThis("Start");
        List<Person> personList = new ArrayList<>();
        for(int index = 0; index < 15;index++) {
            personList.add(PersonFactory.createPerson(Personal.personalType.STUDENT));
        }
        for(int index = 0; index <=3;index++) {
            personList.add(PersonFactory.createPerson(Personal.personalType.TRAINER));
        }

    }
}
