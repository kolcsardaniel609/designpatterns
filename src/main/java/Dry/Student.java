package Dry;

import java.util.*;

public class Student {
    ArrayList<Integer> note;

    public float computeGrade(ArrayList<Integer> note) {
        float grade = 0;
        float sum = 0;
        for (int i = 0; i < note.size(); i++) {
            sum = sum + note.get(i);
        }
        grade = sum/note.size();
        return grade;
    }
}
