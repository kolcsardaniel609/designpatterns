package Dry;

import java.util.*;

public class Application {
    public static void main(String[] args) {
        ArrayList<Integer> note = new ArrayList<Integer>();
        Student student = new Student();
        note.add(10);
        note.add(5);
        System.out.println(student.computeGrade(note));
    }
}
