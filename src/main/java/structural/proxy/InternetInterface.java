package structural.proxy;

public interface InternetInterface {

    public void connectTo(String server) throws Exception;
}
