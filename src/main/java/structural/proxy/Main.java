package structural.proxy;

public class Main {
    public static void main(String[] args) {
        Internet server = new Internet();
        InternetInterface internet = new InternetProxy(server);

        try {
            internet.connectTo("facebook.com");
            internet.connectTo("google.com");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
