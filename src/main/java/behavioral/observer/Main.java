package behavioral.observer;

public class Main {
    public static void main(String[] args) {

        Auctioneer auctioneer = new Auctioneer();

        Bidder bidder1 = new Bidder(auctioneer);
        bidder1.setName("Bidder One");
        bidder1.setMaxBidLimit(101);
        bidder1.setBidStep(11);

        Bidder bidder2 = new Bidder(auctioneer);
        bidder2.setName("Bidder Two");
        bidder2.setMaxBidLimit(202);
        bidder2.setBidStep(22);

        Bidder bidder3 = new Bidder(auctioneer);
        bidder3.setName("Bidder Three");
        bidder3.setMaxBidLimit(303);
        bidder3.setBidStep(33);

        Bidder bidder4 = new Bidder(auctioneer);
        bidder4.setName("Bidder Four");
        bidder4.setMaxBidLimit(404);
        bidder4.setBidStep(44);

        bidder2.bid(100);

        System.out.println("Winner is: " + auctioneer.getWinningBidder());



    }
}
