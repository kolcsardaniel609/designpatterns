package fluentinterface;

public class Restaurant implements IRestaurant {

    private String name;

    public Restaurant(String name) {
        this.name = name;
    }

    @Override
    public IRestaurant select() {
        System.out.println("Enter restaurant: " + this.name);
        return this;
    }

    @Override
    public IMenu getMenu() {
        return null;
    }
}
