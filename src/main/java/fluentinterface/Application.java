package fluentinterface;

import java.util.ArrayList;
import java.util.Arrays;

public class Application {
    public static void main(String[] args) {

        new Restaurant("Primavera")
                .select()
                .getMenu()
                .orderPizza(Arrays.asList(new Integer[]{1,3}))
                .eatPizza()
                .payPizza();
    }
}
