package fluentinterface;

public interface IPizza {
    public IPizza select();

    public IPizza getIngredients();

    public Integer getCost();
}
