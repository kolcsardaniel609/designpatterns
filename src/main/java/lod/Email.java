package lod;

public class Email {
    String email;

    Email(String email) {
        this.email = email;

    }

    public String getEmail() {
        return email;
    }

    @Override
    public String toString() {
        return "Email{" +
                "email='" + email + '\'' +
                '}';
    }
}
