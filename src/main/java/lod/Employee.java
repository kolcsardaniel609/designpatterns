package lod;

public class Employee {
    private String name;
    private Email email;

//    Employee(String name, Email email){
//        this.name = name;
//        this.email = email;
//    }

    public String getName() {
        return name;
    }

//    public Email getEmail() {
//        return email;
//    }

    public void setEmail(Email email) {
        this.email = email;
    }

    public void setName(String name) {
        this.name = name;
    }
    public boolean isEmployeeValid () {
        if(this.email == null || this.email.getEmail().isEmpty()) {
            return false;
        }
       return true;
    }
}
