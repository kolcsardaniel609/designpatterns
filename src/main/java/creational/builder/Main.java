package creational.builder;

public class Main {
    public static void main(String[] args) {
        House voidHouse = new House.HouseBuilder().build();

        House completeHouse = new House.HouseBuilder()
                .addRooms(5)
                .addWindows(10)
                .addGarage(true)
                .addGarden(new Garden())
                .build();

        House simpleHouse = new House.HouseBuilder()
                .addRooms(3)
                .addWindows(7)
                .addGarden(new Garden())
                .build();
        System.out.println(completeHouse.toString());
    }
}
