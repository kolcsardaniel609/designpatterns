package creational.factorymethod;

import java.util.Random;

public class Main {
    public static void main(String[] args) {
        new Random().nextInt(100);
//        Integer id1 = new Random().nextInt();
//        Personal student1 = new Student(id1);
//
//        Integer id2 = new Random().nextInt();
//        Personal student2 = new Student(id2);
//
//        Integer id3 = new Random().nextInt();
//        Personal teacher1 = new Student(id3);

        Personal student1 = PersonalFactory.createPersonal(
                Personal.PersonalType.STUDENT);

        Personal student2 = PersonalFactory.createPersonal(
                Personal.PersonalType.STUDENT);

        Personal teacher1 = PersonalFactory.createPersonal(
                Personal.PersonalType.TEACHER);

    }
}
